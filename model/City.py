# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 00:31:34 2022

@author: Baptiste
"""

class City(object):
    def __init__(self, id, name, state, country, capital=False, population=0,
                 regions=[]):
        self.id = id
        self.name = name
        self.state = state
        self.country = country
        self.capital = capital
        self.population = population
        self.regions = regions

    def __repr__(self):
        return(
            f'City(\
                name={self.name}, \
                country={self.country}, \
                population={self.population}, \
                capital={self.capital}, \
                regions={self.regions}\
            )'
        )
    
    def to_dict(self):
        return self.__dict__

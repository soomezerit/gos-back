# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 22:12:33 2022

@author: Baptiste
"""
class users(object):
    def __init__(self, id, fname, lname, img, equipments=[]):
        self.id = id
        self.fname = fname
        self.lname = lname
        self.img = img
        self.equipments = equipments

    def __repr__(self):
        return(
            f'users(\
                id={self.id}, \
                fname={self.fname}, \
                lname={self.lname}, \
                img={self.img}, \
                equipments={self.equipments}\
            )'
        )
    
    def to_dict(self):
        '''r = self.__dict__
        del r["id"]
        return r'''
        return self.__dict__

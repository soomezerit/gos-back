# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 00:31:34 2022

@author: Baptiste
"""

class equipments(object):
    def __init__(self, id, name, maxQ, actuQ):
        self.id = id
        self.name = name
        self.maxQ = maxQ
        self.actuQ = actuQ
        

    def __repr__(self):
        return(
            f'equipments(\
                id={self.id},\
                name={self.name}, \
                max_quantity={self.country}, \
                actual_quantity={self.population} \
            )'
        )
    
    def to_dict(self):
        '''r = self.__dict__
        del r["id"]
        return r'''
        return self.__dict__

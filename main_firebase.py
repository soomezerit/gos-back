# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 20:24:05 2022

@author: Baptiste
"""

# Import the Firebase service
import firebase_admin
from firebase_admin import credentials, firestore, storage

#import os


class City(object):
    def __init__(self, name, state, country, capital=False, population=0,
                 regions=[]):
        self.name = name
        self.state = state
        self.country = country
        self.capital = capital
        self.population = population
        self.regions = regions

    def __repr__(self):
        return(
            f'City(\
                name={self.name}, \
                country={self.country}, \
                population={self.population}, \
                capital={self.capital}, \
                regions={self.regions}\
            )'
        )
    
    def to_dict(self):
        return self.__dict__

def main():

    #os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="C:/Users/Baptiste/Documents/BM/Dev/Firebase/firststep-622b7-firebase-adminsdk-ovow1-61b7852ba8.json"
    
    
    # Use the application default credentials
    cred = credentials.Certificate("C:/Users/Baptiste/Documents/BM/Dev/Firebase/firststep-622b7-firebase-adminsdk-ovow1-61b7852ba8.json")
    
    try:
        firebase_admin.get_app()
    except:
        firebase_admin.initialize_app(cred, {'storageBucket': 'firststep-622b7.appspot.com'})

    mainStorage()

def mainStorage():
    
    # Init firebase with your credentials
    #cred = credentials.Certificate("YOUR DOWNLOADED CREDENTIALS FILE (JSON)")
    #firebase_admin.initialize_app(cred, {'storageBucket': 'firststep-622b7.appspot.com'})
    
    # Put your local file path 
    
    fileName = "dofus-2020-11-01_22-36-25-Inf-irmiere.png"
    bucket = storage.bucket()
    
    blob = bucket.blob(fileName)
   
    blob.upload_from_filename("{}{}".format("uploads/",fileName))
    #blob.make_public()
    #blob.make_private()
    
    #blob.download_to_filename(fileName)
    
    print("your file url", blob.public_url)

def mainFirestore():
    db = firestore.client()

    users_ref = db.collection(u'cities')
    docs = users_ref.stream()

    finalJson={"cities":[]}

    for doc in docs:
        finalJson["cities"].append({doc.id:doc.to_dict()})
        #print("{} => {}".format(doc.id,doc.to_dict()))

    print(finalJson)

if __name__ == '__main__':
    main()

'''cities_ref = db.collection(u'cities')

cities_ref.document(u'BJ').set(City('Beijing', None, 'China', True, 21500000, ['hebei']).__dict__)'''

        
    

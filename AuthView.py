# -*- coding:utf-8 -*-

'''
Created on 26 nov. 2019

@author: Baptiste
'''

from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager
from resources import AuthRessources

from controller.AuthController import AuthController

app = Flask(__name__)
api = Api(app)
authController = AuthController()
AuthRessources.authCotroller = authController
#app.config['SECRET_KEY'] = 'some-secret-string'

@app.before_first_request
def create_tables():
    authController.initTables()
    
app.config['JWT_SECRET_KEY'] = AuthRessources.smoke
jwt = JWTManager(app)

app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return authController.tokenIsBlackList(jti)

api.add_resource(AuthRessources.UserRegistration, '/registration')
api.add_resource(AuthRessources.UserLogin, '/login')
api.add_resource(AuthRessources.UserLogoutAccess, '/logout/access')
api.add_resource(AuthRessources.UserLogoutRefresh, '/logout/refresh')
api.add_resource(AuthRessources.TokenRefresh, '/token/refresh')
api.add_resource(AuthRessources.AllUsers, '/users')
api.add_resource(AuthRessources.SecretResource, '/secret')

app.run(host='0.0.0.0', port=5003, debug=True)

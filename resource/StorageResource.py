# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 20:35:42 2022

@author: Baptiste
"""

#from flask import send_file
from flask_restful import Resource, reqparse
import werkzeug

parser = reqparse.RequestParser()

parser.add_argument('file', type=werkzeug.datastructures.FileStorage, location='files')

uploadExtension = ['.jpg', '.png', '.gif', '.PNG', '.jpeg']
storageCtrl = None
send_file = None

class UploadFiles(Resource):
    def post(self):
        
        data = parser.parse_args()
        res = {"response":None}
        code = 200
        uploaded_file = data['file']
        res = storageCtrl.storeFile(uploaded_file, uploadExtension)
                
        if(res.get("error_code")):
            code = res.get("error_code")
        
        return res, code

class GetFiles(Resource):
    def get(self):
        
        allFiles = storageCtrl.getAllFiles()
        return {"files": allFiles}

class GetFile(Resource):
    def get(self, filename):
        
        file_like_object, ext = storageCtrl.getFileFromName(filename)
        return send_file(file_like_object, mimetype="image/{}".format(ext))
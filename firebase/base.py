# -*- coding: utf-8 -*-
"""
Created on Sun Mar 13 21:26:53 2022

@author: Baptiste
"""

import firebase_admin


class firebase(object):
    
    def __init__(self, storageData = None):
        
        self.storageData = storageData 
    
    def connect(self):
        
        try:
            
            firebase_admin.get_app()
            
        except:
            
            cred = firebase_admin.credentials.Certificate("firebase/conn/data/firststep-622b7-firebase-adminsdk-ovow1-61b7852ba8.json")
            
            if (self.storageData != None):
                firebase_admin.initialize_app(cred, {'storageBucket': 'firststep-622b7.appspot.com'})
            else:
                firebase_admin.initialize_app(cred)
                
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 00:36:48 2022

@author: Baptiste
"""

from firebase_admin import storage
from firebase.base import firebase
from generic.Tools import Tools

from view.StorageView import StorageView
from resource import StorageResource

import datetime
import requests

class StorageController(object):
    
    def __init__(self, storageName="firststep-622b7.appspot.com"):
        
        #Firebase Constructor
        self.storageName = storageName
        firebase({'storageBucket': self.storageName}).connect()
        self.bucket = storage.bucket()
        
        #View & Resource Constructor
        StorageResource.storageCtrl = self
        strV = StorageView(StorageResource)
        
        #App data
        self.app = strV.app

    def getAllFiles(self):
        
        allFiles = []
        all_blobs = self.bucket.list_blobs()
        
        for file in all_blobs:  
              
            allFiles.append(file.name)
            
        return allFiles
    
    def getFileFromName(self, filename="unknown.png"):
        
        blob = self.bucket.blob(filename)
        img = blob.generate_signed_url(datetime.timedelta(seconds=10), method='GET')
        
        r = requests.get(img, stream=True)
        
        file_like_object = r.raw
        
        ext = Tools().getFileType(filename=filename)
            
        return file_like_object, ext
    
    def storeFile(self, uploaded_file, requiredExt):
        
        filename = Tools().getSecureFilename(uploaded_file.filename)
        
        bucket = storage.bucket()
        blob = bucket.blob(filename)
        
        if filename != '':
            file_ext = Tools().getExtensionFromFilename(filename)
            
            if (file_ext in requiredExt) :
                
                i = uploaded_file.read()
                ext = Tools().getFileType(file_ext=file_ext)
                blob.upload_from_string(i, content_type="image/{}".format(ext))
                res = {"response": "Image downloaded",
                       "filename":filename}         
                
            else:   
                res = {"response":"error",
                       "error_message":"Invalid type",
                       "error_code" :400}
                
        return res
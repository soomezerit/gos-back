# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 23:21:01 2022

@author: Baptiste
"""

from firebase_admin import firestore
from firebase.base import firebase

class FirestoreController(object):
    
    def __init__(self):
        
        firebase().connect()
        self.db = firestore.client()
        
    
    def getDataFromCollectionAndId(self, id, collection):
        doc_ref = self.getCollection(collection).document(id)
        
        return doc_ref
    
    def getCollection(self, collection):
        doc_ref = self.db.collection(collection)
        return doc_ref
    
    def getDataFromCollection(self, collection):
        
        users_ref = self.getCollection(collection)
        docs = users_ref.stream()
        #finalJson={"cities":{}}

        finalJson={collection:[]}

        for doc in docs:
            #finalJson["cities"][doc.id] = doc.to_dict()
            data = doc.to_dict()
            data["id"] = doc.id
            finalJson[collection].append(data)
            
            #print("{} => {}".format(doc.id,doc.to_dict()))
        #print (data)
        return finalJson
        
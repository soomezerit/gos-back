# -*- coding: utf-8 -*-
"""
Created on Sun Mar 13 21:57:05 2022

@author: Baptiste
"""

from werkzeug.utils import secure_filename
import os

class Tools(object):
    '''
    classdocs
    '''
    
    def getSecureFilename(self, filename):
        
        return secure_filename(filename)
    
    def getFileType(self, file_ext="",filename=""):
        if(file_ext == ""):
            file_ext = self.getExtensionFromFilename(filename)
        ext = file_ext.replace(".", "").lower()
        
        return ext
    
    def getExtensionFromFilename(self, filename):
        file_ext = os.path.splitext(filename)[1]
        
        return file_ext
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 23:41:01 2022

@author: Baptiste
"""

from werkzeug.serving import run_simple
from app import application

run_simple('0.0.0.0', 5000, application, use_reloader=True, use_debugger=True
           ,threaded=True)
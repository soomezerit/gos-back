# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 19:54:11 2022

@author: Baptiste
"""

#from generic.parser.ParserJson import ParserJson
import ast

from flask import Flask, jsonify, request, send_file
import requests
#from werkzeug.utils import secure_filename

#import requests
#import io
#import datetime

#import os
#from controller.StorageController import StorageController
from controller.FirestoreController import FirestoreController
from model.Equipments import equipments
from model.Users import users

#from firebase.base import firebase
#from generic.Tools import Tools

# Import the Firebase service
# import firebase_admin
#from firebase_admin import firestore
#import os

app = Flask(__name__)
app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.png', '.gif', '.PNG', '.jpeg']

@app.route('/')
def hello():
    return"Hello World"

@app.route('/data')
def names():
        
    frCtrl = FirestoreController()
    data = frCtrl.getDataFromCollection("cities")

    return (jsonify(data))

@app.route('/users', methods=['GET'])
def get_users():
        
    frCtrl = FirestoreController()
    data = frCtrl.getDataFromCollection("users")

    return (jsonify(data))

@app.route('/user', methods=['GET'])
def get_user():
        
    id = request.args.get('id')
 
    frCtrl = FirestoreController()
    db = frCtrl.db
    
    doc_ref = db.collection('users').document(id)
    doc = doc_ref.get()
    user = doc.to_dict()

    return (jsonify(user))

@app.route('/users', methods=['POST'])
def set_user():
        
    id = request.form.get('id')
    fname =  request.form.get('fname')
    lname =  request.form.get('lname')
    img =  request.form.get('img')
    equipments =  request.form.get('equipments') or []
    
    if(type(equipments) != list):
        equipments = ast.literal_eval(equipments)
    
    '''eq = {"ba": False,
          "bl": False,
          "bs": False,
          "cl": False,
          "cs": False,
          "cv": False,
          "dm": False,
          "gi": False,
          "gp": False,
          "ko": False,
          "lt": False,
          "ms": False,
          "ta": False,
          "tw": False}
    
    for d in equipments:
        print(d)
        if(d in eq):
            eq[d] = True'''
            
    user  = users(id, fname, lname, img, equipments)
    userd = user.to_dict()
    
    frCtrl = FirestoreController()
    db = frCtrl.db
    
    us_ref = db.collection('users')
    us_ref.document(userd.get("id")).set(userd)
    
    return jsonify(userd)

'''@app.route('/users', methods=['PUT'])
def update_user():
        
    id = request.form.get('id')
    equipments =  request.form.get('equipments') or []
    
    if(type(equipments) != list):
        equipments = ast.literal_eval(equipments)
    
    eq = {"ba": False,
          "bl": False,
          "bs": False,
          "cl": False,
          "cs": False,
          "cv": False,
          "dm": False,
          "gi": False,
          "gp": False,
          "ko": False,
          "lt": False,
          "ms": False,
          "ta": False,
          "tw": False}
    
    for d in equipments:
        print(d)
        if(d in eq):
            eq[d] = True
    
    frCtrl = FirestoreController()
    db = frCtrl.db
    
    us_ref = db.collection('users')
    us_ref.document(id).set({'equipments':equipments})
    
    return jsonify({"id":id,
                    "equipments":equipments})'''

@app.route('/users', methods=['PUT'])
def update_user_equipments():

    data = request.json

    id = data.get("userId")
    equipments =  data.get("equipements")

    print(id)
    print(equipments)
    

    if(type(equipments) != list):
        equipments = ast.literal_eval(equipments)
    
    frCtrl = FirestoreController()

    us_ref = frCtrl.getDataFromCollectionAndId(id, 'users')
    
    us = us_ref.get().to_dict()
    us_eq = us.get("equipments")
    
    to_add = list(set(equipments) - set(us_eq))
    to_remove = list(set(us_eq) - set(equipments))
    new_eq = equipments
    
    for add in to_add:
        requests.put("http://localhost:5000/equipments/get",
                     data={"id":add})
    
    for remove in to_remove:
        requests.put("http://localhost:5000/equipments/return",
                     data={"id":remove})
        if remove in new_eq: new_eq.remove(remove)

    us_ref.set({'equipments':new_eq})
    
    new_eq = frCtrl.getDataFromCollectionAndId(id, 'users').get().to_dict().get("equipments")
    
    return jsonify({"id":id,
                    "equipments":new_eq})

@app.route('/equipments', methods=['GET'])
def get_equipments():
        
    frCtrl = FirestoreController()
    data = frCtrl.getDataFromCollection("equipments")

    return (jsonify(data))

@app.route('/equipment', methods=['GET'])
def get_equipment():
        
    id = request.args.get('id')
 
    frCtrl = FirestoreController()
    db = frCtrl.db
    
    doc_ref = db.collection('equipments').document(id)
    doc = doc_ref.get()
    user = doc.to_dict()

    return (jsonify(user))

'''@app.route('/equipment', methods=['GET'])
def get_equipment_new():
        
    id = request.args.get('id')
 
    frCtrl = FirestoreController()
    user = frCtrl.getDataFromCollectionAndId(id, 'equipments').get().to_dict()
    db = frCtrl.db
    
    doc_ref = db.collection('equipments').document(id)
    doc = doc_ref.get()
    user = doc.to_dict()

    return (jsonify(user))'''

@app.route('/equipments', methods=['POST'])
def set_equipments():
    
    id = request.form.get('id')
    name =  request.form.get('name')
    maxQ =  request.form.get('maxQ')
    actuQ =  request.form.get('actuQ')
    
    eq  = equipments(id, name, maxQ, actuQ)
    eqd = eq.to_dict()
    frCtrl = FirestoreController()
    db = frCtrl.db
    
    eq_ref = db.collection('equipments')
    eq_ref.document(eqd.get("id")).set(eqd)
    
    return jsonify(eqd)

'''@app.route('/equipments', methods=['POST'])
def set_equipment_new():
    
    id = request.form.get('id')
    name =  request.form.get('name')
    maxQ =  request.form.get('maxQ')
    actuQ =  request.form.get('actuQ')
    
    eq  = equipments(id, name, maxQ, actuQ)
    eqd = eq.to_dict()
    frCtrl = FirestoreController()
    #db = frCtrl.db
    
    eq_ref = frCtrl.getCollection('equipments')
    eq_ref.document(eqd.get("id")).set(eqd)
    
    return jsonify(eqd)'''


@app.route('/equipments/<order>', methods=['PUT'])
def update_get_equipments(order):
    
    if(order == "get" or order == "return"):
    
        id = request.form.get('id')
        nb = request.form.get('nb') or 1
     
        frCtrl = FirestoreController()
        db = frCtrl.db
        
        doc_ref = db.collection('equipments').document(id)
        doc = doc_ref.get()
        eq = doc.to_dict()
        
        actuQ = eq.get("actuQ")
        
        if(order == "get"):
            nactuQ = int(actuQ) + int(nb)
        elif(order == "return"):
            nactuQ = int(actuQ) - int(nb)
            
        doc_ref.update({"actuQ":nactuQ})
        
        eqd = {"id":id,
               "actuQ":nactuQ}
    
        return jsonify(eqd)
    else:
        return(jsonify({"response":"error"}))

'''@app.route('/equipments/return', methods=['PUT'])
def update_return_equipments():
    
    id = request.form.get('id')
    nb = request.form.get('nb') or 1
 
    frCtrl = FirestoreController()
    db = frCtrl.db
    
    doc_ref = db.collection('equipments').document(id)
    doc = doc_ref.get()
    eq = doc.to_dict()
    
    actuQ = eq.get("actuQ")
    nactuQ = int(actuQ) - int(nb)
    
    doc_ref.update({"actuQ":nactuQ})
    
    eqd = {"id":id,
           "actuQ":nactuQ}
    
    return jsonify(eqd)'''
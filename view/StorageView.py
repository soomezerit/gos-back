# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 20:33:05 2022

@author: Baptiste
"""

from flask import Flask, send_file
from flask_restful import Api

#from resource import StorageResource
#from controller.StorageController import StorageController
#from firebase.base import firebase

class StorageView(object):
    
    def __init__(self, StorageResource):
                
        self.app = Flask(__name__)
        self.send_file = send_file
        api = Api(self.app)
        
        #StorageResource.storageCtrl = storageCtrl
        StorageResource.send_file = send_file
        
        api.add_resource(StorageResource.UploadFiles, '/')
        api.add_resource(StorageResource.GetFiles, '/')
        api.add_resource(StorageResource.GetFile, '/<filename>')
        
        #app.run(host='0.0.0.0', port=5000, debug=True)
        
        
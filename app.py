# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 23:33:29 2022

@author: Baptiste
"""
from werkzeug.middleware.dispatcher import DispatcherMiddleware # use to combine each Flask app into a larger one that is dispatched based on prefix

from test_main import app as fl1
#from view.StorageView import app as fl2
from controller.StorageController import StorageController

strCtrl = StorageController()

application = DispatcherMiddleware(fl1, {
    '/upload': strCtrl.app
})